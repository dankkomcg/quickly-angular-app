const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack'); //to access built-in plugins

// to minification files on prod mode
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');

module.exports = {
    entry: {
        app: './src/App.js'
    },
    output: {
        filename: 'assets/js/[name].[hash].js',
        path: path.resolve(__dirname, 'dist/public')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.txt$/, use: 'raw-loader'
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: process.env.NODE_ENV === 'development',
                        },
                    },
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },

            /** Load Fonts  */
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                // loader: 'url-loader?limit=100000&name=fonts/[name].[ext]'
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // if less than 10 kb, add base64 encoded image to css
                            limit: 100000,
                            // if more than 10 kb move to this folder in build using file-loader
                            name: "fonts/[name].[ext]",
                            // ruta publica partiendo de output.path (bundle) leído desde el css (../)
                            publicPath: "../"
                        }
                    }]
            },

            /** Load images */
            {
                test: /\.(png|jpg|jpeg|gif|svg)$/,
                // loader: 'url-loader?limit=100000',
                use: [
                    {
                        // en este caso url-loader sólo aplica sobre los archivos con formato [img]
                        loader: 'url-loader',
                        options: {
                            // asignar la ruta a través del nombre
                            // hace que el outputPath y publicPath apunten a la misma ruta
                            // evitar utilizar el hash para no crear archivos css tan largos
                            name: "assets/img/[name].[ext]"
                            // limit: 100000
                        }
                    }]
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader',
                    options: {
                        // https://medium.freecodecamp.org/learn-webpack-by-example-blurred-placeholder-images-4ad8b1751709?gi=172e8697abf4
                        // require images from html
                        interpolate: true,
                        minimize: true,
                        removeComments: true,
                        useShortDoctype: true
                    }
                }],
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({ template: './src/index.html' }),
        // https://github.com/webpack-contrib/mini-css-extract-plugin/issues/73
        new MiniCssExtractPlugin({
            filename: `assets/css/[name].[hash].css`
        }),
        // https://stackoverflow.com/questions/37651015/webpack-using-bootstrap-jquery-is-not-defined
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ],
    // https://stackoverflow.com/questions/31191884/set-up-webpack-to-run-locally-on-a-custom-domain-over-https
    // https://webpack.js.org/configuration/dev-server/
    devServer: {
        contentBase: path.join(__dirname, 'srv-public/html'),
        compress: true,
        port: 8081,
        host: "127.0.0.1"
    },

    optimization: {
        /*
        splitChunks: {
            chunks: 'all'
        },^
        */
        minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },

    // https://github.com/webpack-contrib/css-loader/issues/447
    node: {
        fs: 'empty'
    }
};