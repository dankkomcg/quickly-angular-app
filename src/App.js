import Angular from 'angular';
import Router from '@uirouter/angularjs';
import App from './Components/App/app';

// routes
import RouterIndex from './Components/Index/index';

let ngApp = Angular.module('ngApp', [Router])
    .config(['$stateProvider', '$locationProvider', '$urlRouterProvider',
        function ($stateProvider, $locationProvider, $urlRouterProvider) {

            // eliminar # del enrrutado
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

            // add routes
            $stateProvider.state(RouterIndex);

            // route by default
            $urlRouterProvider.otherwise('/');

        }])
    .component(`app`, App);

ngApp.run([() => {

    console.log(`app`);

}]);
